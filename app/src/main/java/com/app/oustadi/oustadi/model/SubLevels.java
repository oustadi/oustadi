package com.app.oustadi.oustadi.model;

/**
 * Created by Majid on 11/06/2016.
 */
public class SubLevels {

    private String id_sublevel;
    private String description;

    public SubLevels() {
    }

    public String getId_sublevel() {
        return id_sublevel;
    }

    public void setId_sublevel(String id_sublevel) {
        this.id_sublevel = id_sublevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}

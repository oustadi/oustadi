package com.app.oustadi.oustadi.model;

/**
 * Created by bb on 23/05/2016.
 */
public class ProfesseurT {

    private String status ;
    private String message;
    private Professeur professeur;

    public ProfesseurT() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Professeur getProfesseur() {
        return professeur;
    }

    public void setProfesseur(Professeur professeur) {
        this.professeur = professeur;
    }
}

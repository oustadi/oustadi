package com.app.oustadi.oustadi.model;

import java.io.Serializable;

/**
 * Created by majid on 01/12/2015.
 */
public class Offre implements Serializable {
    private Integer id_offre;
    private String description;
    private String titre;
    private String prix;
    private String professeur;
    private String nomprofesseur;
    private String prenomprofesseur;
    private String infoprof;
    private String mailprof;
    private String telprof;
    private String date_creation;
    private String photo;

    public Offre() {
    }

    public Offre(Integer id_offre, String description, String titre, String prix,
                 String professeur, String nomprofesseur, String prenomprofesseur,
                 String infoprof, String mailprof, String telprof, String date_creation, String photo) {
        this.id_offre               = id_offre;
        this.description            = description;
        this.titre                  = titre;
        this.prix                   = prix;
        this.professeur             = professeur;
        this.nomprofesseur          = nomprofesseur;
        this.prenomprofesseur       = prenomprofesseur;
        this.infoprof               = infoprof;
        this.mailprof               = mailprof;
        this.telprof                = telprof;
        this.date_creation          = date_creation;
        this.photo                  = photo;
    }

    public Integer getId_offre() {
        return id_offre;
    }

    public void setId_offre(Integer id_offre) {
        this.id_offre = id_offre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getProfesseur() {
        return professeur;
    }

    public void setProfesseur(String professeur) {
        this.professeur = professeur;
    }

    public String getMailprof() {
        return mailprof;
    }

    public void setMailprof(String mailprof) {
        this.mailprof = mailprof;
    }

    public String getTelprof() {
        return telprof;
    }

    public void setTelprof(String telprof) {
        this.telprof = telprof;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getNomprofesseur() {
        return nomprofesseur;
    }

    public void setNomprofesseur(String nomprofesseur) {
        this.nomprofesseur = nomprofesseur;
    }

    public String getPrenomprofesseur() {
        return prenomprofesseur;
    }

    public void setPrenomprofesseur(String prenomprofesseur) {
        this.prenomprofesseur = prenomprofesseur;
    }

    public String getDate_creation() {
        return date_creation;
    }

    public void setDate_creation(String date_creation) {
        this.date_creation = date_creation;
    }

    public String getInfoprof() {
        return infoprof;
    }

    public void setInfoprof(String infoprof) {
        this.infoprof = infoprof;
    }

    public String getNameProf() {

        return nomprofesseur+" "+prenomprofesseur ;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

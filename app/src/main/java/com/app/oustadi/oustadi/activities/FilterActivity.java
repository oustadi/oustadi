package com.app.oustadi.oustadi.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Utils.NetworkHelper;
import com.app.oustadi.oustadi.Views.MyToolBar;
import com.app.oustadi.oustadi.model.Domains;
import com.app.oustadi.oustadi.model.FilterData;
import com.app.oustadi.oustadi.model.Levels;
import com.app.oustadi.oustadi.model.SubLevels;
import com.app.oustadi.oustadi.model.Subjects;
import com.app.oustadi.oustadi.model.Sections;
import com.app.oustadi.oustadi.model.Cities;
import com.app.oustadi.oustadi.webservices.impl.ServiceManager;

import java.util.ArrayList;
import java.util.List;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btn_send, btn_publish;
    private Spinner spinner_city, spinner_area, spinner_subject, spinner_matiere, spinner_domaines, spinner_niveau;

    private FilterData filterData;
    private List<Domains> domainsList;
    private List<Cities> citiesList;
    private List<Levels> levelsList;

    private ArrayAdapter<Levels> adapter_niveaux;
    private ArrayAdapter<SubLevels> adapter_subject;
    private ArrayAdapter<Cities> adapter_city;
    private ArrayAdapter<Sections> adapter_area;
    private ArrayAdapter<Domains> adapter_domaines;
    private ArrayAdapter<Subjects> adapter_matieres;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        context = this;
        init();
        initToolbar();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initToolbar() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.toolbar);

        //definir notre toolbar en tant qu'actionBar
        setSupportActionBar(toolbar);

        //afficher le bouton retour
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }

    public void init() {

        spinner_city = (Spinner) findViewById(R.id.spinner_city);
        spinner_area = (Spinner) findViewById(R.id.spinner_area);
        spinner_matiere = (Spinner) findViewById(R.id.spinner_matiere);
        spinner_subject = (Spinner) findViewById(R.id.spinner_subject);
        spinner_domaines = (Spinner) findViewById(R.id.spinner_domaine);
        spinner_niveau = (Spinner) findViewById(R.id.spinner_level);


        btn_send = (Button) findViewById(R.id.btn_send);
        btn_publish = (Button) findViewById(R.id.btn_publish);

        btn_send.setOnClickListener(this);
        btn_publish.setOnClickListener(this);

        filterData = new FilterData();
        citiesList = new ArrayList<>();

        if (NetworkHelper.isOnline(context))
            new LevelAsync(getApplicationContext()).execute();
        else
            Toast.makeText(getApplicationContext(), context.getResources().getString(R.string.check_connectivity), Toast.LENGTH_LONG).show();

        // spinners listners
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Cities cities = (Cities) ((Spinner) findViewById(R.id.spinner_city)).getSelectedItem();
                List<Sections> quartiers = new ArrayList<Sections>();

                if (cities.getSections() != null) {

                    quartiers = cities.getSections();

                } else {
                    Sections q = new Sections();
                    q.setDescription(cities.getDescription());
                    quartiers.add(q);
                }

                adapter_area = new ArrayAdapter(getApplicationContext(), R.layout.spinner_item, quartiers);
                spinner_area.setAdapter(adapter_area);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_domaines.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                Domains domaine = (Domains) ((Spinner) findViewById(R.id.spinner_domaine)).getSelectedItem();
                adapter_matieres = new ArrayAdapter(getApplicationContext(), R.layout.spinner_item, domaine.getSubjects());
                spinner_matiere.setAdapter(adapter_matieres);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_niveau.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Levels levels = (Levels) ((Spinner) findViewById(R.id.spinner_level)).getSelectedItem();

                adapter_subject = new ArrayAdapter(getApplicationContext(), R.layout.spinner_item, levels.getSublevels());
                spinner_subject.setAdapter(adapter_subject);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:

                Intent mainIntent = new Intent(FilterActivity.this, MainActivity.class);
                startActivity(mainIntent);
                break;

            case R.id.btn_publish:
                Intent addIntent = new Intent(FilterActivity.this, LoginActivity.class);
                startActivity(addIntent);
                break;
        }
    }


    private class LevelAsync extends AsyncTask<Void, Void, Void> {

        Context context;

        public LevelAsync(Context context) {
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {

            filterData = (new ServiceManager()).getLevels();

            if (filterData != null) {
                citiesList          = filterData.getCities();
                domainsList         = filterData.getDomains();
                levelsList          = filterData.getLevels();

                adapter_city        = new ArrayAdapter(context, R.layout.spinner_item, citiesList);
                adapter_domaines    = new ArrayAdapter(context, R.layout.spinner_item, domainsList);
                adapter_niveaux     = new ArrayAdapter(context, R.layout.spinner_item, levelsList);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (filterData != null) {

                spinner_city.setAdapter(adapter_city);
                spinner_domaines.setAdapter(adapter_domaines);
                spinner_niveau.setAdapter(adapter_niveaux);

            }
        }
    }

}

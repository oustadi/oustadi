package com.app.oustadi.oustadi.adapter;

import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Utils.Constant;
import com.app.oustadi.oustadi.Utils.DateFormats;
import com.app.oustadi.oustadi.activities.DetailOffreActivity;
import com.app.oustadi.oustadi.dialogs.CallDialog;
import com.app.oustadi.oustadi.model.Offre;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by majid on 01/12/2015.
 */
public class AdapterOffreList  extends RecyclerView.Adapter<AdapterOffreList.ViewHolder>  {

    private List<Offre> offreList;
    private Offre offre;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private ImageView user_picture, image_menu;

        private TextView offre_title, text_price_offre;
        private TextView offre_desc, text_date_offre;
        private CircleImageView  circleImageView;

        private RelativeLayout rlItem;
        public ViewHolder(View v) {
            super(v);

            user_picture            = (ImageView) v.findViewById(R.id.image_user);

            rlItem                  = (RelativeLayout)  v.findViewById(R.id.rl_item);
            offre_title             = (TextView)        v.findViewById(R.id.text_title_offre);
            offre_desc              = (TextView)        v.findViewById(R.id.text_desc_offre);
            text_date_offre         = (TextView)        v.findViewById(R.id.text_date_offre);
            text_price_offre        = (TextView)        v.findViewById(R.id.text_price_offre);

            image_menu              = (ImageView)       v.findViewById(R.id.image_menu);
            circleImageView         = (CircleImageView) v.findViewById(R.id.image_user);

        }


    }

    public AdapterOffreList(List<Offre> offreList, Context context) {
        this.offreList  = offreList;
        this.context    = context;
    }

    @Override
    public AdapterOffreList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offre, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(AdapterOffreList.ViewHolder holder,final int position) {
        offre = offreList.get(position);


        holder.offre_title.setText(offre.getTitre());
        holder.offre_desc.setText(offre.getDescription());
        holder.text_price_offre.setText(offre.getPrix()+" DH");
        holder.text_date_offre.setText("Publiée le : "+ DateFormats.changeFormatToDate(offre.getDate_creation()));
        Picasso.with(context)
                .load(offre.getPhoto())
                .resize(50, 50)
                .placeholder(R.drawable.people)
                .centerCrop()
                .into(holder.circleImageView);

        holder.rlItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDetail(offreList.get(position));
            }
        });

        holder.image_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(view, offreList.get(position));
            }
        });
    }
    public void goToDetail(Offre off){
        Intent intent = new Intent(context, DetailOffreActivity.class);
        intent.putExtra(Constant.EXTRA_OFFRE,off);
        context.startActivity(intent);
    }

    private void showPopupMenu(View view,final Offre offer) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_item_offre, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_call:
                        // Call popup menu listener passing the menu item that was clicked as well as the recycler view item position:
                        new CallDialog(offre.getTelprof(), context);
                        return true;

                    case R.id.action_open :
                        goToDetail(offer);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    @Override
    public int getItemCount() {
        return offreList.size();
    }
}

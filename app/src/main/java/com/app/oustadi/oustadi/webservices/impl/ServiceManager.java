package com.app.oustadi.oustadi.webservices.impl;

import android.util.Log;

import com.app.oustadi.oustadi.Utils.ErrorUtils;
import com.app.oustadi.oustadi.Utils.LogOstd;
import com.app.oustadi.oustadi.model.Article;
import com.app.oustadi.oustadi.model.JsonError;
import com.app.oustadi.oustadi.model.FilterData;
import com.app.oustadi.oustadi.model.Offre;
import com.app.oustadi.oustadi.model.ProfesseurT;
import com.app.oustadi.oustadi.webservices.interfac.ParseJson;


import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bb on 16/04/2016.
 */
public class ServiceManager {

    // builder rettrofit

    Article article;
    ParseJson service = ServiceGenerator.createService(ParseJson.class);


    public Article getArticle() {

        article = new Article();
        try {

            Call<Article> articleCall = service.getArticle();
            articleCall.enqueue(new Callback<Article>() {
                @Override
                public void onResponse(Call<Article> call, Response<Article> response) {
                    // Log.d("RR",response.isSuccessful()+" resp"+response.body().getTitre());
                    if (response.isSuccessful()) {

                        article = response.body();
                        LogOstd.d(response.body().toString());
                        //  Log.d("desc response", article.getBody());
                    } else {
                        JsonError error = ErrorUtils.parseError(response);

                        // Log.d("error message", error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Article> call, Throwable t) {
                    Log.d("failure message", t.getMessage());
                }
            });

            return article;
        } catch (Exception e) {
            Log.e("error catch ", e.getMessage());
            e.printStackTrace();
        }

        return null;

    }

    public String addOffre(Offre offre) {

        try {
            service.addOffre(offre).enqueue(new Callback<Offre>() {
                @Override
                public void onResponse(Call<Offre> call, Response<Offre> response) {
                    if (response.isSuccessful())
                        LogOstd.d(response.message().toString());
                    else
                        Log.d("offre message", response.isSuccessful() + "");
                }

                @Override
                public void onFailure(Call<Offre> call, Throwable t) {
                    Log.d("error message", t.getMessage());
                }
            });

        } catch (Exception e) {
            Log.e("error catch ", e.getMessage());
            e.printStackTrace();
        }
        return "";

    }

    public List<Offre> getOffres() {

        Call<List<Offre>> offres = service.getOffres();


        try {

            return offres.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ProfesseurT loginProf(String mail, String password) {

        try {
            Call<ProfesseurT> professeurCall = service.loginProf(mail, password);
            return professeurCall.execute().body();

        } catch (Exception e) {

            e.printStackTrace();
        }
        return null;

    }

    public FilterData getLevels() {

        FilterData lv = new FilterData();
        Call<FilterData> filterData = service.getFilterData();

        try {
            lv = filterData.execute().body();
            return lv;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

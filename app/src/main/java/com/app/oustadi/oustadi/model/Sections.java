package com.app.oustadi.oustadi.model;

/**
 * Created by Majid on 05/06/2016.
 */
public class Sections {
    private String id_section;
    private String description;


    public Sections() {
    }

    public String getId_section() {
        return id_section;
    }

    public void setId_section(String id_section) {
        this.id_section = id_section;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}

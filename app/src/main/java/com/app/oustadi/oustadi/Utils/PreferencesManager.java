package com.app.oustadi.oustadi.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Majid on 06/06/2016.
 */
public class PreferencesManager {

    public static void putStringPref(Context mContext, String key, String value) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(Constant.SHARED_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getStringPref(Context mContext, String key) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(Constant.SHARED_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPref.getString(key, null);

    }

    public static void putBooleenPref(Context mContext, String key, String value) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(Constant.SHARED_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean getBooleenPref(Context mContext, String key) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(Constant.SHARED_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(key, false);

    }
}

package com.app.oustadi.oustadi.model;

import java.util.List;

/**
 * Created by Majid on 05/06/2016.
 */
public class Domains {

    private String id_domain;
    private String description;
    private List<Subjects> subjects;

    public Domains() {
    }

    public String getId_domain() {
        return id_domain;
    }

    public void setId_domain(String id_domain) {
        this.id_domain = id_domain;
    }

    public List<Subjects> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subjects> subjects) {
        this.subjects = subjects;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Override
    public String toString() {
        return this.description;
    }
}

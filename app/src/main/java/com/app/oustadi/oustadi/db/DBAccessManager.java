package com.app.oustadi.oustadi.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.app.oustadi.oustadi.services.DBProvider;

import java.util.List;

/**
 * Created by QGER on 20-04-15.
 */
public class DBAccessManager {
    private static final String SELECT = "SELECT ";
    private static final String FROM = " FROM ";
    private static final String WHERE = " WHERE ";
    private static final String AND = " AND ";
    private static final String OR = " OR ";
    private static final String GROUPBY = " GROUPBY ";
    private static final String HAVING = " HAVING ";


    //INSERT
    public static Long insertOneLine(String table, ContentValues values) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        return db.insert(table, null, values);
    }

    public static void insertMultipleLine(String table, List<ContentValues> valuesList) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        db.beginTransaction();
        for (ContentValues values : valuesList){
            insertOneLine(table,values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    //UPDATE
    public static void updateOneLine(String table, ContentValues values, String nameColumnID) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        db.update(table, values, nameColumnID + " = ?", new String[]{values.getAsString(nameColumnID)});
    }

    public static void updateMultipleLine(String table, List<ContentValues> valuesList, String nameColumnID) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        db.beginTransaction();
        for (ContentValues values : valuesList){
            updateOneLine(table, values,nameColumnID);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    //DELETE
    public static Integer deleteWithID(String table, String nameColumnID, Long id) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        return db.delete(table, nameColumnID + " = ? ", new String[]{id.toString()});
    }

    //QUERY
    //uuid directly in place of string tab , string tab doesn't run for uuid
    public static Cursor queryWithUID(String table, String[] column, String fieldName, String uid) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        return db.query(table, column, fieldName + "='" + uid + "'", null, null, null, null);
    }
    public static Cursor queryWithID(String table, String[] column, String fieldName, Integer id) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        return db.query(table, column, fieldName + "=" + id + "", null, null, null, null);
    }

    public static void execSQL(String query) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        db.execSQL(query);
    }

    public static Cursor rawQueryExec(String query) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        return db.rawQuery(query, null);
    }

    public static Integer queryForCountTable(String table, String nameColumnID,String deleteColumnName,Boolean isDeleted) {
        SQLiteDatabase db = DBProvider.getInstance().getDB();
        String deleteValue = isDeleted?"1":"0";
        String query = "SELECT COUNT("+nameColumnID+") AS cnt FROM "+table+" WHERE "+deleteColumnName+"="+deleteValue+";";
        Cursor cursor = rawQueryExec(query);
        cursor.moveToFirst();
        Integer count = cursor.getInt(cursor.getColumnIndex("cnt"));
        cursor.close();
        return count;
    }
}

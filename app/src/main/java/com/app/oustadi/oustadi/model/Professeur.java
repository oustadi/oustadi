package com.app.oustadi.oustadi.model;

/**
 * Created by bb on 20/04/2016.
 */
public class Professeur {

    private String ID_PROF;
    private String NOMPROF;


    public Professeur() {
    }

    public String getID_PROF() {
        return ID_PROF;
    }

    public void setID_PROF(String ID_PROF) {
        this.ID_PROF = ID_PROF;
    }

    public String getNOMPROF() {
        return NOMPROF;
    }

    public void setNOMPROF(String NOMPROF) {
        this.NOMPROF = NOMPROF;
    }


}

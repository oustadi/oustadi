package com.app.oustadi.oustadi.model;

/**
 * Created by Majid on 05/06/2016.
 */
public class Subjects {

    private String id_subject;
    private String description;

    public Subjects() {
    }

    public String getId_subject() {
        return id_subject;
    }

    public void setId_subject(String id_subject) {
        this.id_subject = id_subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}

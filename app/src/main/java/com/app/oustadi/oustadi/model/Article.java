package com.app.oustadi.oustadi.model;

/**
 * Created by bb on 16/04/2016.
 */
public class Article {

    private String  titre;
    private String  body;
    private String  prix;

    public Article() {
    }

    public Article(String titre, String body, String prix) {
        this.titre = titre;
        this.body = body;
        this.prix = prix;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }
}

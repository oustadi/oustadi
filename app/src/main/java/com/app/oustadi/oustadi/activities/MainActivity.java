package com.app.oustadi.oustadi.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Utils.Constant;
import com.app.oustadi.oustadi.Utils.NetworkHelper;
import com.app.oustadi.oustadi.Views.MyToolBar;
import com.app.oustadi.oustadi.adapter.AdapterOffreList;
import com.app.oustadi.oustadi.interfaces.RecyclerItemClickListener;
import com.app.oustadi.oustadi.model.Article;
import com.app.oustadi.oustadi.model.Offre;
import com.app.oustadi.oustadi.webservices.impl.ServiceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView offreListRV;
    private RecyclerView.Adapter rvAdapter;
    private RecyclerView.LayoutManager rvLayoutManager;

    private List<Offre> offreList;
    private Article article;
    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        // init toolBar
        initToolbar();


        // get article json
        checkOffers();

        // init list offre
        offreListRV = (RecyclerView) findViewById(R.id.offre_list);
        offreListRV.setHasFixedSize(true);

        // use a linear layout manager
        rvLayoutManager = new LinearLayoutManager(getApplicationContext());
        offreListRV.setLayoutManager(rvLayoutManager);
        // offreList   =   getOffreList();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, AddOffreActivity.class);
                startActivity(intent);
            }
        });


    }

    public void initToolbar() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.toolbar);

        //definir notre toolbar en tant qu'actionBar
        setSupportActionBar(toolbar);

        toolbar.setTitle("Offres");
        //afficher le bouton retour
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

            case R.id.action_update:
                checkOffers();
                break;

            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        // SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setQueryHint("Matière...");

        // searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                makeSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                makeSearch(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void makeSearch(String query) {
        query = query.toLowerCase();

        // prepare result of search
        final List<Offre> list = new ArrayList<>();

        for (Offre offre : offreList) {

            String text = offre.getTitre() != null ? offre.getTitre() : "";
            if (text.toLowerCase().contains(query)) {
                list.add(offre);
            }
        }
        rvAdapter = new AdapterOffreList(list, activity);
        offreListRV.setAdapter(rvAdapter);
    }

    public void setListnner(Offre offre) {
        Intent intent = new Intent(activity, DetailOffreActivity.class);
        intent.putExtra(Constant.EXTRA_OFFRE, offre);
        startActivity(intent);
    }

    public List<Offre> getOffreList() {
        List<Offre> list = new ArrayList<>();


        return list;
    }

    public void checkOffers() {

        if (NetworkHelper.isOnline(activity))
            (new GetJson()).execute();
        else
            Toast.makeText(getApplicationContext(), activity.getResources().getString(R.string.check_connectivity), Toast.LENGTH_LONG).show();
    }

    private class GetJson extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        protected void onPreExecute() {
            dialog.setMessage("Chargement...");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            offreList = (new ServiceManager()).getOffres();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //  Log.d("Title : ",article.getTitre());

            if (offreList != null) {
                rvAdapter = new AdapterOffreList(offreList, activity);
                offreListRV.setAdapter(rvAdapter);
            }

            dialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }
}

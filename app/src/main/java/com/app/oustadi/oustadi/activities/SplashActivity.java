package com.app.oustadi.oustadi.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.app.oustadi.oustadi.R;

public class SplashActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                // Create an Intent that will start the MainActivity.

                Intent filterIntent = new Intent(SplashActivity.this,FilterActivity.class);
                startActivity(filterIntent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}

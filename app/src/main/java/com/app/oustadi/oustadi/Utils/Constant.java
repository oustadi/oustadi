package com.app.oustadi.oustadi.Utils;

/**
 * Created by majid on 02/12/2015.
 */
public class Constant {

    public static final String EXTRA_OFFRE                      = "extraoffre";
    public static final String BASE_URL                         = "https://app-oustadi.rhcloud.com/index.php/";
    public static final String SHARED_PREFERENCE                = "shared_preferences";
    public static final String OUSTADI                          = "OUSTADI";
}

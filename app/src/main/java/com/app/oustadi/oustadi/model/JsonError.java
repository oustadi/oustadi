package com.app.oustadi.oustadi.model;

/**
 * Created by bb on 16/04/2016.
 */
public class JsonError {

    private String status ;
    private String message;

    public JsonError() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

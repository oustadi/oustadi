package com.app.oustadi.oustadi.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Utils.Constant;
import com.app.oustadi.oustadi.Utils.DateFormats;
import com.app.oustadi.oustadi.dialogs.CallDialog;
import com.app.oustadi.oustadi.model.Offre;

/**
 * Created by majid on 02/12/2015.
 */


public class DetailOffreActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView text_publish_date, text_prof_name, text_email, text_tel, text_price, text_offre_desc, text_city;
    private Offre offre;
    private ImageView btnCall, btnSendMail, btnSms;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_offre);
        context = this;
        initToolbar();

        offre               = (Offre) getIntent().getSerializableExtra(Constant.EXTRA_OFFRE);

        text_publish_date   = (TextView) findViewById(R.id.text_publish_date);
        text_prof_name      = (TextView) findViewById(R.id.text_prof_name);
        text_email          = (TextView) findViewById(R.id.text_email);
        text_tel            = (TextView) findViewById(R.id.text_tel);
        text_price          = (TextView) findViewById(R.id.text_price);
        text_offre_desc     = (TextView) findViewById(R.id.text_offre_desc);
        text_city           = (TextView) findViewById(R.id.text_city);

        btnCall              = (ImageView) findViewById(R.id.btnCall);
        btnSendMail          = (ImageView) findViewById(R.id.btnSendMail);
        btnSms               = (ImageView) findViewById(R.id.btnSms);

        // init listners views
        btnCall.setOnClickListener(this);
        btnSendMail.setOnClickListener(this);
        btnSms.setOnClickListener(this);

        text_publish_date.setText("Publiée le : "+DateFormats.changeFormatToDate(offre.getDate_creation()));
        text_offre_desc.setText(offre.getDescription());
        text_prof_name.setText(offre.getNameProf().toUpperCase());
        text_email.setText("Email : "+offre.getMailprof());
        text_tel.setText("Tél : "+offre.getTelprof());
        text_city.setText("Ville : Casablanca");
        text_price.setText("Prix : "+offre.getPrix());


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch ( id ) {

            case android.R.id.home :
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        //definir notre toolbar en tant qu'actionBar
        setSupportActionBar(toolbar);

        toolbar.setTitle("Détail offre");

        //afficher le bouton retour
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }
    public void sendMail(){
        String[] TO = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

        emailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{offre.getMailprof()});

        try {
           startActivity(Intent.createChooser(emailIntent, "Select Email Client"));
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }
    public void sendSMS() {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + offre.getTelprof()));
        intent.putExtra("sms_body", "Merci de me contacter");
        startActivity(intent);
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnCall :

                new CallDialog(offre.getTelprof(), context);
                break;

            case R.id.btnSendMail :
                sendMail();
                break;

            case R.id.btnSms :
                sendSMS();
                break;
        }
    }


}



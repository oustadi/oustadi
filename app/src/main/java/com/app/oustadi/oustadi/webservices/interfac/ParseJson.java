package com.app.oustadi.oustadi.webservices.interfac;

import com.app.oustadi.oustadi.model.Article;
import com.app.oustadi.oustadi.model.FilterData;
import com.app.oustadi.oustadi.model.Offre;
import com.app.oustadi.oustadi.model.ProfesseurT;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by bb on 16/04/2016.
 */
public interface ParseJson {

    @GET("api/getListOffre")
    Call<Article> getArticle();

    @GET("api/offres")
    Call<List<Offre>> getOffres();

    @POST("api/addoffre")
    @Headers({ "Content-Type: application/json"})
    Call<Offre> addOffre(@Body  Offre offre) ;

    @FormUrlEncoded
    @POST("api/signinprof")
    Call<ProfesseurT> registerProf(@Field("nom") String nom, @Field("prenom") String prenom, @Field("password") String password  , @Field("mail") String mail, @Field("infos") String infos) ;

    @FormUrlEncoded
    @POST("api/login")
    Call<ProfesseurT> loginProf(@Field("mail") String mail, @Field("password") String password) ;

    @POST("api/filterData")
    @Headers({ "Content-Type: application/json"})
    Call<FilterData> getFilterData();
}

package com.app.oustadi.oustadi.adapter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

/**
 * Created by majid on 02/10/2015.
 */
public class OnItemClickListenerListViewItem implements OnItemClickListener {

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Context context = view.getContext();

        // just toast it
        Toast.makeText(context, "Item: " + position+ ", Item ID: " , Toast.LENGTH_SHORT).show();



    }

}
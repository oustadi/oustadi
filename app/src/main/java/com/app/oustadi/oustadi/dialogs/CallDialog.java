package com.app.oustadi.oustadi.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.app.oustadi.oustadi.R;

/**
 * Created by bb on 23/05/2016.
 */
public class CallDialog {


    private String number;
    private Context context;

    public CallDialog(String number, Context context) {
        this.number = number;
        this.context = context;
        showAlert();
    }

    public void showAlert() {

        new AlertDialog.Builder(context)
                .setTitle("Appeler professeur")
                .setMessage("Voulez-vous appelez le professeur?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
                        context.startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }
}

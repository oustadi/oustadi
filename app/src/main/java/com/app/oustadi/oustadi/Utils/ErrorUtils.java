package com.app.oustadi.oustadi.Utils;

import com.app.oustadi.oustadi.model.JsonError;
import com.app.oustadi.oustadi.webservices.impl.ServiceGenerator;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;


/**
 * Created by bb on 16/04/2016.
 */
public class ErrorUtils {

    public static JsonError parseError(Response<?> response) {

        Converter<ResponseBody, JsonError> converter = ServiceGenerator.retrofit()
                        .responseBodyConverter(JsonError.class, new Annotation[0]);

        JsonError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new JsonError();
        }

        return error;
    }
}

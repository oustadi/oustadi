package com.app.oustadi.oustadi.Utils;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bb on 23/05/2016.
 */
public class DateFormats {

    public static String changeFormatToDate(String date){

        try {
            // Parse the input date
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date inputDate = fmt.parse(date);

            // Create the MySQL datetime string
            fmt = new SimpleDateFormat("dd-MM-yyyy à HH:mm");


            return fmt.format(inputDate);

        } catch (Exception e){
            return null;
        }
                }
}

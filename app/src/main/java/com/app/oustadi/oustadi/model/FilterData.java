package com.app.oustadi.oustadi.model;


import java.util.List;

public class FilterData {

    private String status;
    private String message;

    public List<Levels> levels;
    private List<Cities> cities;
    private List<Domains> domains;
    public FilterData() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Levels> getLevels() {
        return levels;
    }

    public void setLevels(List<Levels> levels) {
        this.levels = levels;
    }

    public List<Cities> getCities() {
        return cities;
    }

    public void setCities(List<Cities> cities) {
        this.cities = cities;
    }

    public List<Domains> getDomains() {
        return domains;
    }

    public void setDomains(List<Domains> domains) {
        this.domains = domains;
    }

    @Override
    public String toString() {
        return this.status;
    }
}
package com.app.oustadi.oustadi.model;

import java.util.List;

/**
 * Created by Majid on 05/06/2016.
 */
public class Cities {

    private String id_city;
    private String description;
    private List<Sections> sections;

    public Cities() {
    }

    public String getId_city() {
        return id_city;
    }

    public void setId_city(String id_city) {
        this.id_city = id_city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Sections> getSections() {
        return sections;
    }

    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    @Override
    public String toString() {
        return this.description;
    }
}

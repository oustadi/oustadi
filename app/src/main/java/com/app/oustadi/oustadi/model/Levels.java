package com.app.oustadi.oustadi.model;

import java.util.List;

/**
 * Created by Majid on 05/06/2016.
 */
public class Levels {

    private String id_level;
    private String description;
    private List<SubLevels> sublevels;

    public Levels() {
    }

    public String getId_level() {
        return id_level;
    }

    public void setId_level(String id_level) {
        this.id_level = id_level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SubLevels> getSublevels() {
        return sublevels;
    }

    public void setSublevels(List<SubLevels> sublevels) {
        this.sublevels = sublevels;
    }

    @Override
    public String toString() {
        return this.description;
    }
}

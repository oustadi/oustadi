package com.app.oustadi.oustadi.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.app.oustadi.oustadi.R;

/**
 * Created by bb on 09/05/2016.
 */
public class MyButton extends Button {

    // Default colours/styles
    private int myButtonDrawable = R.drawable.rounded_button;
    private int myButtonTextColor = R.color.white;

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setBackgroundResource(myButtonDrawable);
        this.setTextColor(getResources().getColor(myButtonTextColor));
    }


    public MyButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}

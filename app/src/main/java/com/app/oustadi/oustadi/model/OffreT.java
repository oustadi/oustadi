package com.app.oustadi.oustadi.model;

import java.util.List;

/**
 * Created by bb on 18/04/2016.
 */
public class OffreT {

    private List<Offre> offres;

    public List<Offre> getOffres() {
        return offres;
    }

    public void setOffres(List<Offre> offres) {
        this.offres = offres;
    }
}

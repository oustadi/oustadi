package com.app.oustadi.oustadi.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.app.oustadi.oustadi.Utils.NetworkHelper;

/**
 * Created by Majid on 06/06/2016.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        boolean status = NetworkHelper.isOnline(context);

        Toast.makeText(context, "isOnline : "+status, Toast.LENGTH_LONG).show();
    }
}
package com.app.oustadi.oustadi.Utils;

import android.util.Log;

/**
 * Created by Majid on 06/06/2016.
 */
public class LogOstd  {

    public  LogOstd() {

    }

    public static void v(String message){

        Log.v(Constant.OUSTADI, message);
    }
    public static void d(String message){
        Log.d(Constant.OUSTADI, message);
    }
    public static void e(String message){
        Log.e(Constant.OUSTADI, message);
    }
    public static void i(String message){
        Log.i(Constant.OUSTADI, message);
    }
    public static void w(String message){
        Log.w(Constant.OUSTADI, message);
    }

}

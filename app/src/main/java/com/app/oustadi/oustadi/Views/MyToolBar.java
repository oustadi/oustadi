package com.app.oustadi.oustadi.Views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.ContextMenu;

import com.app.oustadi.oustadi.R;

/**
 * Created by bb on 20/04/2016.
 */
public class MyToolBar extends Toolbar {

    private int mytoolbarDrawable = R.color.colorApp600;


    @Override
    protected void onCreateContextMenu(ContextMenu menu) {
        super.onCreateContextMenu(menu);


    }

    public MyToolBar(Context context) {
        super(context);
    }

    public MyToolBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setBackgroundResource(mytoolbarDrawable);
    }

    public MyToolBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }
}

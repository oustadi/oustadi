package com.app.oustadi.oustadi.services;


import android.database.sqlite.SQLiteDatabase;


/**
 * Created by QGER on 21-04-15.
 * Provides access to the SQLLite database instance, to be used by DAOs
 */
public class DBProvider {
    private static SQLiteDatabase db;
    private static DBProvider instance = null;

    private DBProvider(){
    }

    public synchronized static DBProvider getInstance() {
        if (instance == null) {
            instance = new DBProvider();
        }
        return instance;
    }

    public synchronized static SQLiteDatabase getDB() {
        if (db == null) {
          //  db = new IZDBHelper().getWritableDatabase();
        }
        return db;
    }
}

package com.app.oustadi.oustadi.activities;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Views.MyToolBar;
import com.app.oustadi.oustadi.model.Offre;
import com.app.oustadi.oustadi.webservices.impl.ServiceManager;

/**
 * Created by bb on 16/04/2016.
 */
public class AddOffreActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText edit_price, edit_desc;
    private Button btn_send;
    private Spinner spinner_city, spinner_level, spinner_subject;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offre);

        init();
        initToolbar();

    }

  public void init() {

      spinner_city          = (Spinner)  findViewById(R.id.spinner_city);
      spinner_level         = (Spinner)  findViewById(R.id.spinner_level);
      spinner_subject       = (Spinner)  findViewById(R.id.spinner_subject);

      edit_price            = (EditText) findViewById(R.id.edit_price);
      edit_desc             = (EditText) findViewById(R.id.edit_desc);

      btn_send              = (Button) findViewById(R.id.btn_send);

      btn_send.setOnClickListener(this);


  }

    @Override
    protected void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edit_price.getWindowToken(), 0);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch ( id ) {

            case android.R.id.home :
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void initToolbar() {
        MyToolBar toolbar = (MyToolBar) findViewById(R.id.toolbar);

        //definir notre toolbar en tant qu'actionBar
        setSupportActionBar(toolbar);

        //afficher le bouton retour
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_send :
                // send json to hajaji

                (new SenJson()).execute();

                break;
        }
    }

    private class SenJson extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Offre offre = new Offre();
            offre.setDescription("test");
            offre.setPrix("150");
            (new ServiceManager()).addOffre(offre);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            finish();
            super.onPostExecute(aVoid);
        }
    }

}

package com.app.oustadi.oustadi.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.app.oustadi.oustadi.R;
import com.app.oustadi.oustadi.Views.MyButton;

/**
 * Created by bb on 23/05/2016.
 */
public class SendMailDialog extends Dialog implements View.OnClickListener{

    private Context context;
    private EditText edit_subject, edit_body;
    private MyButton send;

    public SendMailDialog(Context context) {
        super(context);
        this.context = context;
        show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_send_mail);

        edit_body           = (EditText) findViewById(R.id.edit_body);
        edit_subject        = (EditText) findViewById(R.id.edit_subject);
        send                = (MyButton) findViewById(R.id.btn_send_mail);

        send.setOnClickListener(this);
    }

    public void sendMail(){
        String[] TO = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

        emailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"nahli.gmajid@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, edit_subject.getText().toString());
        emailIntent.putExtra(Intent.EXTRA_TEXT   , edit_body.getText().toString());
        try {
            context.startActivity(Intent.createChooser(emailIntent, "Select Email Client"));
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_send_mail :
                sendMail();
                dismiss();
                break;

        }
    }
}
